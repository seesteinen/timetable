package ru.company.timetable;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.company.timetable.dto.EventDto;
import ru.company.timetable.dto.EventForm;
import ru.company.timetable.dto.UserDto;
import ru.company.timetable.dto.UserForm;
import ru.company.timetable.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class TimetableApplicationTests {

    TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    void contextLoads() {
    }

    @Test
    public void addUser() {
        UserForm userForm = UserForm.builder()
                .email("petrova@mail.ru")
                .firstName("Мария")
                .lastName("Петрова")
                .build();
        ResponseEntity<UserDto> response = restTemplate.postForEntity("http://localhost:8087/v1/users", userForm, UserDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getFirstName(), is("Мария"));
        assertThat(response.getBody().getLastName(), is("Петрова"));
        assertThat(response.getBody().getEmail(), is("petrova@mail.ru"));
    }

    @Test
    public void addUser2() {
        UserForm userForm = UserForm.builder()
                .email("petrov.egor@mail.ru")
                .firstName("Егор")
                .lastName("Петров")
                .build();
        ResponseEntity<UserDto> response = restTemplate.postForEntity("http://localhost:8087/v1/users",
                userForm, UserDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getFirstName(), is("Егор"));
        assertThat(response.getBody().getLastName(), is("Петров"));
        assertThat(response.getBody().getEmail(), is("petrov.egor@mail.ru"));
    }

    @Test
    public void addUserWithExistingEmail() {
        UserForm userForm = UserForm.builder()
                .email("petrova@mail.ru")
                .firstName("Мария")
                .lastName("Петрова")
                .build();
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8087/v1/users",
                userForm, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.FOUND));
        assertThat(response.getBody(), is("Пользователь с такой почтой уже существует"));

    }

    @Test
    public void getUserById() {
        ResponseEntity<UserDto> response = restTemplate.getForEntity("http://localhost:8087/v1/users/5", UserDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getEmail(), is("petrova@mail.ru"));
        assertThat(response.getBody().getFirstName(), is("Мария"));
        assertThat(response.getBody().getLastName(), is("Петрова"));
    }

    @Test
    public void getUserByWrongId() throws UserNotFoundException {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8087/v1/users/89", String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), is("Пользователь с id 89 не найден"));
    }

    @Test
    public void getUserByWrongId2() throws UserNotFoundException {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8087/v1/users/a", String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void addEvent() {
        EventForm eventForm = EventForm.builder()
                .description("Тестовая встреча")
                .day(10)
                .month(04)
                .year(2023)
                .startHour(15)
                .startMinute(15)
                .endHour(16)
                .endMinute(30)
                .build();
        ResponseEntity<EventDto> response = restTemplate.postForEntity("http://localhost:8087/v1/events",
                eventForm, EventDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getDescription(), is("Тестовая встреча"));
        assertThat(response.getBody().getStartDate().getTime().toString(), is("Mon Apr 10 15:15:00 UTC 2023"));
        assertThat(response.getBody().getEndDate().getTime().toString(), is("Mon Apr 10 16:30:00 UTC 2023"));
    }

    @Test
    public void addEventWithSameTime() {
        EventForm eventForm = EventForm.builder()
                .description("Тестовая встреча")
                .day(10)
                .month(04)
                .year(2023)
                .startHour(16)
                .startMinute(30)
                .endHour(16)
                .endMinute(30)
                .build();
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8087/v1/events",
                eventForm, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        assertThat(response.getBody(), is("Событие не может длиться 0 минут или заканчиваться раньше, чем начинаться"));
    }

    @Test
    public void addEventWithWrongTime() {
        EventForm eventForm = EventForm.builder()
                .description("Тестовая встреча")
                .day(10)
                .month(04)
                .year(2023)
                .startHour(17)
                .startMinute(30)
                .endHour(16)
                .endMinute(30)
                .build();
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8087/v1/events",
                eventForm, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        assertThat(response.getBody(), is("Событие не может длиться 0 минут или заканчиваться раньше, чем начинаться"));
    }

    @Test
    public void getEventById() {
        ResponseEntity<EventDto> response = restTemplate.getForEntity("http://localhost:8087/v1/events/15",
                EventDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getDescription(), is("Тестовая встреча"));
        assertThat(response.getBody().getStartDate().getTime().toString(), is("Mon Apr 10 15:15:00 UTC 2023"));
        assertThat(response.getBody().getEndDate().getTime().toString(), is("Mon Apr 10 16:30:00 UTC 2023"));
    }

    @Test
    public void getEventByWrongId() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8087/v1/events/155", String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), is("Событие с id 155 не найдено"));
    }

    @Test
    public void getEventByWrongId2() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8087/v1/events/kjcnd", String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void addOneParticipantToEvent() {
        EventDto eventDto = EventDto.builder().build();
        ResponseEntity<EventDto> response = restTemplate.postForEntity("http://localhost:8087/v1/events/15/users/5",
                eventDto, EventDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getDescription(), is("Тестовая встреча"));
        assertThat(response.getBody().getStartDate().getTime().toString(), is("Mon Apr 10 15:15:00 UTC 2023"));
        assertThat(response.getBody().getEndDate().getTime().toString(), is("Mon Apr 10 16:30:00 UTC 2023"));
    }

    @Test
    public void addWrongParticipantToEvent() {
        EventDto eventDto = EventDto.builder().build();
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8087/v1/events/150/users/5",
                eventDto, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), is("Событие с id 150 не найдено"));
    }

    @Test
    public void addOneParticipantToWrongEvent() {
        EventDto eventDto = EventDto.builder().build();
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8087/v1/events/15/users/57",
                eventDto, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), is("Пользователь с id 57 не найден"));
    }

    @Test
    public void addParticipantsToEvent() {
        List<String> participantsEmails = new ArrayList<>();
        participantsEmails.add("petrova@mail.ru");
        participantsEmails.add("petrov.egor@mail.ru");
        ResponseEntity<EventDto> response = restTemplate.postForEntity("http://localhost:8087/v1/events/15/users/",
                participantsEmails, EventDto.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getDescription(), is("Тестовая встреча"));
        assertThat(response.getBody().getStartDate().getTime().toString(), is("Mon Apr 10 15:15:00 UTC 2023"));
        assertThat(response.getBody().getEndDate().getTime().toString(), is("Mon Apr 10 16:30:00 UTC 2023"));
    }

    @Test
    public void addWrongParticipantsToEvent() {
        List<String> participantsEmails = new ArrayList<>();
        participantsEmails.add("petrova1@mail.ru");
        participantsEmails.add("petrov1.egor@mail.ru");
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8087/v1/events/15/users/",
                participantsEmails, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), is("Пользователи с email [petrova1@mail.ru, petrov1.egor@mail.ru] не найдены"));
    }

    @Test
    public void addWrongParticipantsToEvent2() {
        List<String> participantsEmails = new ArrayList<>();
        participantsEmails.add("petrova@mail.ru");
        ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:8087/v1/events/15/users/",
                participantsEmails, String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));
        assertThat(response.getBody(), is(
                "{\"message\":\"Пользователь petrova@mail.ru уже занят в это время. Событие не может быть добавлено\"," +
                        "\"crossing events\":" +
                        "[{\"startDate\":\"2023-04-10T15:15:00.000+00:00\"," +
                        "\"endDate\":\"2023-04-10T16:30:00.000+00:00\"," +
                        "\"description\":\"Тестовая встреча\"}]}"));
    }

    @Test
    public void getFreeTimeByUsersId1() {
        ResponseEntity<String> response = restTemplate.getForEntity(
                "http://localhost:8087/v1/events/free-time?day=23&month=03&year=2022&ids=1,2,3,5,6",
                String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody(), is("[{\"from\":\"2022-03-23T08:00:00.000+00:00\"," +
                "\"to\":\"2022-03-23T08:59:00.000+00:00\"}," +
                "{\"from\":\"2022-03-23T09:31:00.000+00:00\"," +
                "\"to\":\"2022-03-23T10:06:00.000+00:00\"}," +
                "{\"from\":\"2022-03-23T10:28:00.000+00:00\"," +
                "\"to\":\"2022-03-23T10:59:00.000+00:00\"}," +
                "{\"from\":\"2022-03-23T15:11:00.000+00:00\"," +
                "\"to\":\"2022-03-23T16:59:00.000+00:00\"}," +
                "{\"from\":\"2022-03-23T19:01:00.000+00:00\"," +
                "\"to\":\"2022-03-23T22:00:00.000+00:00\"}]"));
    }

    @Test
    public void getFreeTimeByWrongUsersId() {
        ResponseEntity<String> response = restTemplate.getForEntity(
                "http://localhost:8087/v1/events/free-time?day=23&month=03&year=2022&ids=67,89",
                String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), is("Пользователь с id 67 не найден"));
    }

    @Test
    public void getFreeTimeByUsersIdThatHaveNoFreeTime() {
        ResponseEntity<String> response = restTemplate.getForEntity(
                "http://localhost:8087/v1/events/free-time?day=23&month=03&year=2022&ids=1,4",
                String.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        assertThat(response.getBody(), is("У пользователей нет общего свободного времени"));
    }
}
