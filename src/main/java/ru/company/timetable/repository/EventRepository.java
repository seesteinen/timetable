package ru.company.timetable.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.company.timetable.entity.Event;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

public interface EventRepository extends JpaRepository<Event, Long> {
    Optional<Event> findById(Long id);

    @Query(value = "SELECT e.* " +
            "FROM account a " +
            "JOIN event_participants ep ON a.id = ep.user_id " +
            "JOIN event e ON ep.event_id = e.id " +
            "WHERE a.id = ?1 AND (e.start_date <= ?2 AND e.end_date >= ?2" +
            "    OR e.start_date <= ?3 AND e.end_date >= ?3" +
            "    OR e.start_date >= ?2 AND e.end_date <= ?3);",
            nativeQuery = true)
    List<Event> findCrossingEventsByUserIdAndStartDateAndEndDate(Long id, Calendar start, Calendar end);

    @Query(value = "SELECT e.* " +
            " FROM account a " +
            " JOIN event_participants ep ON a.id = ep.user_id " +
            " JOIN event e ON ep.event_id = e.id " +
            " WHERE a.id = ?1 AND e.start_date >= ?2 AND e.end_date <= ?3 ;",
            nativeQuery = true)
    List<Event> findEventsByUserIdAndDayStartDateAndDayEndDate(Long id, Calendar dayStart, Calendar dayEnd);

}
