package ru.company.timetable.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.company.timetable.dto.UserDto;
import ru.company.timetable.dto.UserForm;
import ru.company.timetable.service.UserService;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/v1/users/{user-id}")
    public UserDto getOneUser(@PathVariable("user-id") Long id) {
        return userService.getOneById(id);
    }

    @PostMapping("/v1/users")
    public UserDto addUser(@RequestBody UserForm form) {
        return userService.add(form);
    }

}
