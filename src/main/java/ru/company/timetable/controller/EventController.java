package ru.company.timetable.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.company.timetable.dto.EventDto;
import ru.company.timetable.dto.EventForm;
import ru.company.timetable.dto.FreeTimeDto;
import ru.company.timetable.service.EventService;

import java.util.List;

@RestController
public class EventController {

    @Autowired
    private EventService eventService;


    @GetMapping("/v1/events/{event-id}")
    public EventDto getOneEvent(@PathVariable("event-id") Long id) {
        return eventService.getOneById(id);
    }

    @PostMapping("/v1/events")
    public EventDto addEvent(@RequestBody EventForm form) {
        return eventService.add(form);
    }

    @PostMapping("/v1/events/{event-id}/users/{user-id}")
    public EventDto addOneParticipantToEvent(@PathVariable("user-id") Long id,
                                             @PathVariable("event-id") Long eventId) {
        return eventService.addOneParticipantToEvent(id, eventId);
    }

    @PostMapping("/v1/events/{event-id}/users")
    public EventDto addParticipantsToEvent(@RequestBody List<String> participantsEmails,
                                           @PathVariable("event-id") Long eventId) {
        return eventService.addParticipantsToEvent(eventId, participantsEmails);
    }

    @GetMapping("/v1/events/free-time")
    public List<FreeTimeDto> getFreeTime(@RequestParam Integer day, @RequestParam Integer month,
                                         @RequestParam Integer year, @RequestParam List<String> ids) {
        return eventService.getFreeTime(ids, day, month, year);
    }
}
