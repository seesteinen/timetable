package ru.company.timetable.exception;

import lombok.Getter;
import ru.company.timetable.dto.EventDto;

import java.util.List;

@Getter
public class EventJoinException extends RuntimeException {
    private List<EventDto> crossingEvents;
    public EventJoinException(String message, List<EventDto> crossingEvents) {
        super(message);
        this.crossingEvents = crossingEvents;
    }
}

