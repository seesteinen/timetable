package ru.company.timetable.exception;

public class NoFreeTimeException extends RuntimeException {
    public NoFreeTimeException(String message) {
        super(message);
    }
}