package ru.company.timetable.exception;

public class IllegalTimeArgumentsException extends RuntimeException {
    public IllegalTimeArgumentsException(String message) {
            super(message);
    }
}
