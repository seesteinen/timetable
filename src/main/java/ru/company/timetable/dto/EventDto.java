package ru.company.timetable.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventDto {
    private Calendar startDate;
    private Calendar endDate;
    private String description;
}
