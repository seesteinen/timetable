package ru.company.timetable.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.company.timetable.dto.EventDto;
import ru.company.timetable.dto.EventForm;
import ru.company.timetable.dto.FreeTimeDto;
import ru.company.timetable.entity.Event;
import ru.company.timetable.entity.User;
import ru.company.timetable.exception.*;
import ru.company.timetable.repository.EventRepository;
import ru.company.timetable.repository.UserRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public EventDto add(EventForm form) {
        Calendar start = new GregorianCalendar(form.getYear(), form.getMonth() - 1, form.getDay(),
                form.getStartHour(), form.getStartMinute());
        Calendar end = new GregorianCalendar(form.getYear(), form.getMonth() - 1, form.getDay(),
                form.getEndHour(), form.getEndMinute());
        if (start.before(end)) {
            Event event = Event.builder()
                    .description(form.getDescription())
                    .startDate(start)
                    .endDate(end)
                    .build();
            Event savedEvent = eventRepository.save(event);
            return modelMapper.map(savedEvent, EventDto.class);
        } else {
            throw new IllegalTimeArgumentsException("Событие не может длиться 0 минут или заканчиваться раньше, чем начинаться");
        }
    }

    @Override
    public EventDto getOneById(Long id) {
        Event event = eventRepository.findById(id)
                .orElseThrow(() -> new EventNotFoundException("Событие с id " + id + " не найдено"));
        return modelMapper.map(event, EventDto.class);
    }

    @Override
    public EventDto addOneParticipantToEvent(Long id, Long eventId) {
        Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new EventNotFoundException("Событие с id " + eventId + " не найдено"));
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с id " + id + " не найден"));
        List<Event> crossingEvents = eventRepository
                .findCrossingEventsByUserIdAndStartDateAndEndDate(id, event.getStartDate(), event.getEndDate());
        if (crossingEvents.size() > 0) {
            List<EventDto> crossingEventsDtoList = new ArrayList<>();
            for (Event oneEvent : crossingEvents) {
                crossingEventsDtoList.add(modelMapper.map(oneEvent, EventDto.class));
            }
            throw new EventJoinException("Пользователь уже занят в это время. Событие не может быть добавлено", crossingEventsDtoList);
        } else {
            event.getParticipants().add(user);
            return modelMapper.map(eventRepository.save(event), EventDto.class);
        }
    }

    @Override
    public EventDto addParticipantsToEvent(Long eventId, List<String> participantsEmails) {
        Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new EventNotFoundException("Событие с id " + eventId + " не найдено"));
        List<String> notFoundUsers = new ArrayList<>();
        List<User> foundUsers = new ArrayList<>();
        for (String email : participantsEmails) {
            Optional<User> optionalUser = userRepository.findByEmail(email);
            if (!optionalUser.isPresent()) {
                notFoundUsers.add(email);
            } else if (optionalUser.isPresent() && notFoundUsers.size() < 1) {
                foundUsers.add(optionalUser.get());
            }
        }
        if (notFoundUsers.size() > 0) {
            throw new UserNotFoundException("Пользователи с email " + notFoundUsers + " не найдены");
        } else {
            List<User> usersWithoutCrossingEvents = new ArrayList<>();
            for (User user : foundUsers) {
                List<Event> crossingEvents = eventRepository.findCrossingEventsByUserIdAndStartDateAndEndDate(
                        user.getId(), event.getStartDate(), event.getEndDate());
                if (crossingEvents.size() > 0) {
                    List<EventDto> crossingEventsDtoList = new ArrayList<>();
                    for (Event oneEvent : crossingEvents) {
                        crossingEventsDtoList.add(modelMapper.map(oneEvent, EventDto.class));
                    }
                    throw new EventJoinException("Пользователь " + user.getEmail()
                            + " уже занят в это время. Событие не может быть добавлено", crossingEventsDtoList);
                } else {
                    usersWithoutCrossingEvents.add(user);
                }
            }
            event.getParticipants().addAll(usersWithoutCrossingEvents);
            return modelMapper.map(eventRepository.save(event), EventDto.class);
        }
    }

    @Override
    public List<FreeTimeDto> getFreeTime(List<String> ids, Integer day, Integer month, Integer year) {
        Calendar startOfDay = new GregorianCalendar(year, month - 1, day, 8, 0);
        Calendar endOfDay = new GregorianCalendar(year, month - 1, day, 22, 0);
        FreeTimeDto freeTimeDto = new FreeTimeDto(startOfDay, endOfDay);
        List<FreeTimeDto> freeTimeIntervals = new LinkedList<>();
        freeTimeIntervals.add(freeTimeDto);
        System.out.println(ids);
        for (String userId : ids) {
            userRepository.findById(Long.valueOf(userId)).orElseThrow(
                    () -> new UserNotFoundException("Пользователь с id " + userId + " не найден"));

            List<EventDto> userEventsDto = eventRepository.findEventsByUserIdAndDayStartDateAndDayEndDate(
                    Long.valueOf(userId), startOfDay, endOfDay).stream().map(
                    x -> modelMapper.map(x, EventDto.class)).collect(Collectors.toList());
            for (EventDto event : userEventsDto) {
                for (int i = 0; i < freeTimeIntervals.size(); i++) {
                    FreeTimeDto freeTime = freeTimeIntervals.get(i);
                    if (event.getEndDate().before(freeTime.getFrom()))
                        break;
                    //случай когда событие находится внутри отрезка свободного времени
                    if (event.getStartDate().after(freeTime.getFrom()) && event.getEndDate().before(freeTime.getTo())) {
                        FreeTimeDto firstNewFreeTime = FreeTimeDto.builder()
                                .from(freeTime.getFrom())
                                .to(new GregorianCalendar(event.getStartDate().get(Calendar.YEAR),
                                        event.getStartDate().get(Calendar.MONTH),
                                        event.getStartDate().get(Calendar.DAY_OF_MONTH),
                                        event.getStartDate().get(Calendar.HOUR_OF_DAY),
                                        event.getStartDate().get(Calendar.MINUTE) - 1))
                                .build();
                        FreeTimeDto secondNewFreeTime = FreeTimeDto.builder()
                                .from(new GregorianCalendar(event.getEndDate().get(Calendar.YEAR),
                                        event.getEndDate().get(Calendar.MONTH),
                                        event.getEndDate().get(Calendar.DAY_OF_MONTH),
                                        event.getEndDate().get(Calendar.HOUR_OF_DAY),
                                        event.getEndDate().get(Calendar.MINUTE) + 1))
                                .to(freeTime.getTo())
                                .build();
                        freeTimeIntervals.remove(i);
                        freeTimeIntervals.add(i, firstNewFreeTime);
                        freeTimeIntervals.add(i + 1, secondNewFreeTime);
                        //случай когда событие начинается внутри окошка свободного времени:
                    } else if (event.getStartDate().after(freeTime.getFrom())
                            && event.getStartDate().before(freeTime.getTo())) {
                        FreeTimeDto newFreeTime = FreeTimeDto.builder()
                                .from(freeTime.getFrom())
                                .to(new GregorianCalendar(event.getStartDate().get(Calendar.YEAR),
                                        event.getStartDate().get(Calendar.MONTH),
                                        event.getStartDate().get(Calendar.DAY_OF_MONTH),
                                        event.getStartDate().get(Calendar.HOUR_OF_DAY),
                                        event.getStartDate().get(Calendar.MINUTE) - 1))
                                .build();
                        freeTimeIntervals.remove(i);
                        freeTimeIntervals.add(i, newFreeTime);
                        //случай когда событие заканчивается внутри свободного окошка:
                    } else if (freeTime.getFrom().before(event.getEndDate())
                            && event.getEndDate().before(freeTime.getTo())) {
                        FreeTimeDto newFreeTime = FreeTimeDto.builder()
                                .from(new GregorianCalendar(event.getEndDate().get(Calendar.YEAR),
                                        event.getEndDate().get(Calendar.MONTH),
                                        event.getEndDate().get(Calendar.DAY_OF_MONTH),
                                        event.getEndDate().get(Calendar.HOUR_OF_DAY),
                                        event.getEndDate().get(Calendar.MINUTE) + 1))
                                .to(freeTime.getTo())
                                .build();
                        freeTimeIntervals.remove(i);
                        freeTimeIntervals.add(i, newFreeTime);
                        //случай когда свободное окошко внутри события:
                    } else if ((event.getStartDate().before(freeTime.getFrom()) || event.getStartDate().equals(freeTime.getFrom()))
                            && (event.getEndDate().after(freeTime.getTo()) || event.getEndDate().equals(freeTime.getTo()))) {
                        freeTimeIntervals.remove(i);
                        i--;
                        if (freeTimeIntervals.size() < 1)
                            throw new NoFreeTimeException("У пользователей нет общего свободного времени");
                    }
                }
            }
        }
        return freeTimeIntervals;
    }
}
