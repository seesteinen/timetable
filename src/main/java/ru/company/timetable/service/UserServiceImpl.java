package ru.company.timetable.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.company.timetable.dto.EventDto;
import ru.company.timetable.dto.UserDto;
import ru.company.timetable.dto.UserForm;
import ru.company.timetable.entity.Event;
import ru.company.timetable.entity.User;
import ru.company.timetable.exception.UserAlreadyExistsException;
import ru.company.timetable.exception.UserNotFoundException;
import ru.company.timetable.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserDto add(UserForm userForm) {
        if (userRepository.findByEmail(userForm.getEmail()).isPresent())
            throw new UserAlreadyExistsException("Пользователь с такой почтой уже существует");
        User newUser = modelMapper.map(userForm, User.class);
        User savedUser = userRepository.save(newUser);
        return modelMapper.map(savedUser, UserDto.class);
    }

    @Override
    public UserDto getOneById(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с id " + id + " не найден"));
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }

    @Override
    public UserDto getOneByEmail(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с почтой " + email + " не найден"));
        UserDto userDto = modelMapper.map(user, UserDto.class);
        for (Event event : user.getEvents()) {
            userDto.getEvents().add(modelMapper.map(event, EventDto.class));
        }
        return userDto;
    }


}
