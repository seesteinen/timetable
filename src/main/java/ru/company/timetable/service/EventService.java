package ru.company.timetable.service;

import ru.company.timetable.dto.EventDto;
import ru.company.timetable.dto.EventForm;
import ru.company.timetable.dto.FreeTimeDto;

import java.util.List;

public interface EventService {
    EventDto add(EventForm form);

    EventDto getOneById(Long id);

    EventDto addOneParticipantToEvent(Long id, Long eventId);

    EventDto addParticipantsToEvent(Long eventId, List<String> participantsEmails);

    List<FreeTimeDto> getFreeTime(List<String> ids, Integer day, Integer month, Integer year);
}
