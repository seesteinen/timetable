package ru.company.timetable.service;

import ru.company.timetable.dto.UserDto;
import ru.company.timetable.dto.UserForm;

public interface UserService {
    UserDto add(UserForm userForm);

    UserDto getOneById(Long id);

    UserDto getOneByEmail(String email);
}
